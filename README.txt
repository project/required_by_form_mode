CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

-- INTRODUCTION --
* .

-- REQUIREMENTS --

* none.

-- INSTALLATION --
* Install as usual, see:
  https://www.drupal.org/docs/extending-drupal/installing-modules
  for further information.


-- CONFIGURATION --
* .

-- Maintainers --
Current maintainers:
* Abdulaziz zaid (abdulaziz1zaid) - https://www.drupal.org/u/abdulaziz1zaid
